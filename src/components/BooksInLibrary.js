import React, { Component } from 'react';
import axios from 'axios';

class BooksInLibrary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            books: []
        };
    }

    componentDidMount() {

        axios.get('http://127.0.0.1:8080/api/books/available')
            .then(res => {
                const books = res.data.books;
                // books.forEach(function (book) {
                //     if(book.amount === 0)
                //     {
                //         books.splice(books.indexOf(book),1);
                //     }
                // });
                this.setState({ books });
            });

    }

    render() {
        console.log(this.state.books);
        return (
            <div>
                <div className="container">
                    <section className="section">
                        <h1 className="title">Books available in library</h1>


                        <table className="table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>title</th>
                                    <th>amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.books.map(book=>
                                    <tr key={ book.id }>
                                        <td>{ book.id }</td>
                                        <td>{ book.title }</td>
                                        <td>{ book.amount }</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        );
    }
}

export default BooksInLibrary;