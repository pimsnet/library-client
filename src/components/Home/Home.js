import React, { Component } from 'react';
import './Home.css';
import Parser from 'html-react-parser';

class Home extends Component {
    render() {
        let heading = "library.app React Client";
        let subheading = "This client is done in React and it gets it's data from the Symfony source app! How awesome is that?";
        let paragraph = 'BTW it\'s made by <a href="piotr@bozetka.net">Piotr Bożętka</a>';

        return (
            <div>
                <section className="hero">
                    <div className="hero-body">
                        <div className="container">
                        <h1 className="title">{ heading }</h1>
                        <div className="is-two-thirds column is-paddingless">
                            <h2 className="subtitle is-4">{ subheading }</h2>
                            <p>{ Parser(paragraph) }</p>
                        </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Home;