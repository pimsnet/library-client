import React, { Component } from 'react';
import axios from 'axios';

class BooksInLibrary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            books: []
        };
    }

    componentDidMount() {

        axios.get('http://127.0.0.1:8080/api/books/notavailable')
            .then(res => {
                const books = res.data.books;
                this.setState({ books });
            });

    }

    render() {
        console.log(this.state.books);
        return (
            <div>
                <div className="container">
                    <section className="section">
                        <h1 className="title">Books not available in library</h1>


                        <table className="table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>title</th>
                                    <th>amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.books.map(book=>
                                    <tr key={ book.id }>
                                        <td>{ book.id }</td>
                                        <td>{ book.title }</td>
                                        <td>{ book.amount }</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        );
    }
}

export default BooksInLibrary;