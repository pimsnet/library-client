import React, { Component } from 'react';
import './Menu.css';
import { Link } from 'react-router-dom';

class Menu extends Component {

  constructor(props) {
      super(props);
      this.state = {isToggleOn: false};

      this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
      this.setState(prevState => ({
          isToggleOn: !prevState.isToggleOn
      }));
  }

  render() {

    let menuActive = this.state.isToggleOn ? 'is-active' : '';
    
    return (
          <div className="nav has-shadow">
      <div className="container">
        <div className="nav-left">
          <a className="nav-item">library.app client</a>
        </div>

        <span className={'nav-toggle '+menuActive} onClick={this.handleClick}>
          <span></span>
          <span></span>
          <span></span>
        </span>

        <div className={'nav-right nav-menu '+menuActive}>

          <Link to="/" className="nav-item r-item">Home</Link>
          <Link to="/booksinlibrary" className="nav-item r-item">Books in library</Link>
          <Link to="/booksnotinlibrary" className="nav-item r-item">Books not in library</Link>
          <Link to="/booksamount" className="nav-item r-item">Books that are more then 10 pieces</Link>

        </div>
      </div>
    </div>
    );
  }
}

export default Menu;
