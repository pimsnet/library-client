import React, { Component } from 'react';
import './App.css';
import Menu from './components/Menu/Menu';
import Home from './components/Home/Home';
import { Route } from 'react-router-dom';
import BooksInLibrary from './components/BooksInLibrary';
import BooksNotInLibrary from './components/BooksNotInLibrary';
import BooksAmount from './components/BooksAmount';


class App extends Component {

    render() {
        return (
            <div className="App">

              <Menu />

              <Route exact={true} path="/" component={Home} />
              <Route path="/booksinlibrary" component={BooksInLibrary} />
              <Route path="/booksnotinlibrary" component={BooksNotInLibrary} />
              <Route path="/booksamount" component={BooksAmount} />

            </div>
        );
    }
}

export default App;
